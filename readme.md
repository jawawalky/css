# CSS - Cascading Stylesheets
## What You Will Learn
In this course you will learn

- how to use styling information to change the appearance of *HTML* pages

## What You Need
For this course you need the following programs and tools

- [GIT](https://git-scm.com/) - your version control system
- [Visual Sudio Code](https://code.visualstudio.com/) - your IDE
- [Chrome](https://www.google.com/chrome/download-chrome/), [Firefox](https://www.mozilla.org/en-US/firefox/), etc. - your browser

# Demos and Exercises
The demos and exercises are organized in *GIT* branches. The demo branches start with *demo/...* and the exercise branches start with *exercise/...*

## The Trail
The trail is a good way to learn things step by step. The demos of the trail can be found under *demo/trail/...* and the exercises under *exercise/trail/...*

This is the sequence, in which the trail is organized

1. *demo/trail/intro*
1. *demo/trail/selectors*


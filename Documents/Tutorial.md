# What You Will Learn About
In this lesson you will learn about

- the structure of *CSS* selectors
- *CSS* comments
- how to integrate *CSS* into an *HTML* page
- media-specific style sheets

# CSS Selectors
## What is a CSS Selector?
A *CSS* selector is a definition, which identifies certain parts of an *HTML* page and says, what kind of styling should be applied to it.

There are different kinds of *CSS* selectors, which allow you to refer to different *HTML* syntax elements, such as *HTML* elements, element attributes, element classes, etc.

## Structure of a CSS Selector
A CSS selector is composed in the following way

![css-selector-definition.png](./Images/css-selector-definition.png)

The *Selector* identifies *HTML* part that should be styled. In curly brackets follows the styling information, which consists of key-value-pairs.

> In this example all *HTML* `div` elements will use red text color.

The styling block can contain one or more styling declarations.

So a real world *CSS* definition may look like this

```
h1 {
  font-family: "Helvetica";
  font-size: 250%;
  text-align: center;
}
```

# CSS Comments
As with most source code, it is good practice to leave comments in the source code to help others understand the code or to help us remember, what we once did.

A comment in *CSS* starts with `/*` and ends with `*/`. So with a comment our previous example looks like this

```
/*-------------------------------------------------*/
/*                     Headings                    */
/*-------------------------------------------------*/

h1 {
  font-family: "Helvetica";
  font-size: 250%;
  text-align: center;
}
```

# CSS Integration into HTML
Now we will have a look at how we can integrate CSS into our HTML page. There exist three methods of doing so

- **In-lined Styles** - Styling a single *HTML* element.
- **Internal Style Sheet** - Embedding style sheet into *HTML* page.
- **External Style Sheet** - Defining styles in separate file and referring to it from *HTML* page.

## In-lined Styles
*HTML* elements can be styled directly by writing the *CSS* style definitions into the `style` attribute of the *HTML* element.

So if we had some *HTML* code, we could place our styling information, where it should be applied.

```
<html>
  <head>...</head>
  <body>
    <h1 style="font-family: Helvetica;
               font-size: 250%;
               text-align: center;">
      Styling with CSS
    </h1>
    ...
  </body>
```

## Internal Style Sheet
In the case of an internal style sheet, we embed the styling information by adding a `style` child element to the `head` element.

```
<html>
  <head>
    ...
    <style type="text/css">
      h1 {
        font-family: Helvetica;
        font-size: 250%;
        text-align: center;"
      }
      ...
    </style>
  </head>
  <body>
    <h1>Styling with CSS</h1>
    ...
  </body>
```

> **Note:** With `type="text/css"` we define the format of the styling information.

## External Style Sheet
The most common version is the external style sheet.

- It does not *pollute* the *HTML* with styling information.
- The style sheet can be reused for other *HTML* documents.
- The styling of the *HTML* page can easily be replaced by some other styling, defined in another *CSS* file.

The styling information is stored in a separate *CSS* file. It is added to the *HTML* file by a `link` child element in the the `head` element.

**CSS File**

```
/*-------------------------------------------------*/
/*                     Headings                    */
/*-------------------------------------------------*/

h1 {
  font-family: "Helvetica";
  font-size: 250%;
  text-align: center;
}
...
```
`styles.css`

**HMTL File**

```
<html>
  <head>
    ...
    <link rel="stylesheet" type="text/css" href="styles.css">
  </head>
  <body>
    <h1>Styling with CSS</h1>
    ...
  </body>
```
`index.html`

## Combined Styling
We can combine all three ways of styling in one *HTML* page. It is also possible to link to more than one style sheet file. Styles from different locations referring to the same *HTML* part are combined with each other, in case the do not conflict with each other. In that case the latest definition will win.

> It is best practice to separate *HTML* from *CSS*.

# Media-specific Styles and Style Sheets
Normally an *HTML* page will be displayed on a Web browser. But it might also be printed on a printer or published on a braille device. Further output media are possible. Depending on the output media different layouts and formats might be desirable.

## Specifying a Media Attribute
We can support *media-specific* style sheets by denoting the output target in the `link` element, which imports the *CSS* file into the *HTML* file. We specify different *CSS* files for different purposes.

**Example**

```
<html>
  <head>
    ...
    <link rel="stylesheet" type="text/css" media="screen" href="styles.css">
    <link rel="stylesheet" type="text/css" media="print" href="print.css">
    <link rel="stylesheet" type="text/css" media="braille" href="braille.css">
  </head>
  <body>
    <h1>Styling with CSS</h1>
    ...
  </body>
```
`index.html`

Here are the possible values for the `media` attribute

| Attribute Value | Output Device |
| ----- | -------------------- |
| `all` | all output devices |
| `aural` | output on a screen reader |
| `braille` | a braille line reader for blind people |
| `embossed` | braille-based printer |
| `handheld` | mobile devices |
| `print` | printers |
| `projection` | beamers and like devices |
| `screen` | screen-based devices such as browsers, etc. |
| `speech` | same as `aural` |
| `tty` | terminals without graphical support |
| `tv` | television and like devices |

## Media Rule in CSS
*CSS* knows rules, which begin with `@`. The media rule allows us to define the desired output device for some styling definition. It is called `@media`.

**Example**

```
h1 {
  font-family: "Helvetica";
  font-size: 250%;
  text-align: center;
}
@media print {
  h1 {
    font-family: "Courier";
    font-size: 150%;
    text-align: left;
  }
}
```
`styles.css`
